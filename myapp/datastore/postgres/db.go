package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// db details
const (
	postgres_host     = "dpg-chqnogu7avjb90meo1t0-a.singapore-postgres.render.com"
	postgres_port     = 5432
	postgres_user     = "postgres_db"
	postgres_password = "7sk9wiQCezlp3gjVu4haVNm2vEXT7NrO"
	postgres_dbname   = "my_hosted_db"
)

// create pointer variable Db which points to sql driver
var Db *sql.DB

// init() is always called before main()
func init() {
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error
	//open connection to database
	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}
